import requests
from bs4 import BeautifulSoup as BF
from lxml import html

link = "https://www.noticiasagricolas.com.br/cotacoes/cafe/indicador-cepea-esalq-cafe-arabica"
req = requests.get(link)
tree = html.fromstring(req.content)

cafeArabicaData = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[1]/text()")
cafeArabicaPreco = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[2]/text()")
cafeArabicaVariacao = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[3]/text()")

link = "https://www.noticiasagricolas.com.br/cotacoes/cafe/indicador-cepea-esalq-cafe-conillon"
req = requests.get(link)
tree = html.fromstring(req.content)

cafeConillonData = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[1]/text()")
cafeConillonPreco = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[2]/text()")
cafeConillonVariacao = tree.xpath("/html/body/div[1]/div[4]/section/div[3]/div[3]/div[1]/div[2]/table/tbody/tr/td[3]/text()")


print("Café Arábica:\nData: " + cafeArabicaData[0] + " |   Preço: " + cafeArabicaPreco[0] + "  |   Variação: " + cafeArabicaVariacao[0])
print("Café Conillon:\nData: " + cafeConillonData[0] + " |   Preço: " + cafeConillonPreco[0] + "  |   Variação: " + cafeConillonVariacao[0])